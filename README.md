# libvirt_hypervisor role

Setup a libvirt/kvm hypervisor potentially with ZFS storage.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Dependencies


_This role does not depend on other Ansible roles._


## Role Variables

* `libvirt_hypervisor_zfs_storage`

    Name of zfs file system to use for libvirt storage. This will be
    mounted at `/var/lib/libvirt/images`.

    If not using ZFS, simply leave this variable undefined.

    Default value: Not defined

* `libvirt_hypervisor_bridges`

    A list of bridges to define libvirt network for.

    Each entry correspond to the name of a network bridge on the host.

    Default value: Empty list

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: libvirt_hypervisor
```
